<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * librarynih theme.
 */

/**
*Customize your search form
*/

function librarynih_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibility
    $form['search_block_form']['#default_value'] = t('Search this site...'); // Set a default value for the textfield
    $form['actions']['submit']['#value'] = t('GO!'); // Change the text on the submit button
    $form['actions']['submit']['#attributes']['alt'] = "Search Button"; //add alt tag
    unset($form['actions']['submit']['#value']); // Remove the value attribute from the input tag, since it is not valid when input type = image

    $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/gosearch15.png', '#alt' => 'GO');

// Add extra attributes to the text box
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search this site...';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search this site...') {this.value = '';}";
  }
}

function librarynih_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $crumb_arrow = '<span class="crumbs-arrow"> &rsaquo; </span>';

  if (!empty($breadcrumb)) {
    $arr_crumbs = array();
    array_push($arr_crumbs, '<span class="crumbs">' . implode($crumb_arrow, $breadcrumb) . '</span>');

    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $array_size = count($arr_crumbs);

    for ($i=0; $i < $array_size; $i++) {
      if ( $i == $array_size - 1 ) {
      // Menu link title may override the content title
        (menu_get_active_title()) ? $current_crumb = menu_get_active_title() : $current_crumb = drupal_get_title();
      // If current page is 'Edit Page'
      if (substr(drupal_get_title(), 0, 18) == '<em>Edit Page</em>') {
          $current_crumb = 'Edit';
        }

        $output .= $arr_crumbs[$i] . '<span class="crumbs-current">' . $crumb_arrow . $current_crumb . '</span>';
        break;
      }
      $output .= $arr_crumbs[$i];
    }

    return '<div class="breadcrumb">' . $output . '</div>';
  }
}


function librarynih_date_nav_title($params) {
  $granularity = $params['granularity'];
  $view = $params['view'];
  $date_info = $view->date_info;
  $link = !empty($params['link']) ? $params['link'] : FALSE;
  $format = !empty($params['format']) ? $params['format'] : NULL;
  switch ($granularity) {
    case 'year':
      $title = $date_info->year;
      $date_arg = $date_info->year;
      break;
    case 'month':
      $format = !empty($format) ? $format : (empty($date_info->mini) ? 'F Y' : 'F Y');
      $title = date_format_date($date_info->min_date, 'custom', $format);
      $date_arg = $date_info->year .'-'. date_pad($date_info->month);
      break;
    case 'day':
      $format = !empty($format) ? $format : (empty($date_info->mini) ? 'l, F j Y' : 'l, F j');
      $title = date_format_date($date_info->min_date, 'custom', $format);
      $date_arg = $date_info->year .'-'. date_pad($date_info->month) .'-'. date_pad($date_info->day);
      break;
    case 'week':
        $format = !empty($format) ? $format : (empty($date_info->mini) ? 'F j Y' : 'F j');
      $title = t('Week of @date', array('@date' => date_format_date($date_info->min_date, 'custom', $format)));
        $date_arg = $date_info->year .'-W'. date_pad($date_info->week);
        break;
  }
  if (!empty($date_info->mini) || $link) {
      // Month navigation titles are used as links in the mini view.
    $attributes = array('title' => t('View full page month'));
      $url = date_pager_url($view, $granularity, $date_arg, TRUE);
    return l($title, $url, array('attributes' => $attributes));
  }
  else {
    return $title;
  }
}

/**
 * Implements hook__preprocess_block().
 */
function librarynih_preprocess_block(&$variables, $hook) {
  // allow to use block templates depend on delta
  // example: delta = main-menu ; template file: block--main-menu.tpl.php
 if ($variables['block']->delta == 'menu-quick-links') {
    $variables['theme_hook_suggestions'][] = 'block__' . str_replace('-', '_', $variables['block']->delta);
 }
}

/**
 * Main menu
 * Implements theme__menu_tree().
 */
function librarynih_menu_tree__menu_quick_links($variables) {
    global $level;
    $div = ($level == 1) ? '<div class="more-link"><a href="/training/list">More Resources >></a></div>' : '';
    $ul = '<ul class="menu">' . $variables['tree'] . '</ul>' . $div;
    return $ul;
}

/**
 * Main menu
 * Implements theme__menu_link().
 */
function librarynih_menu_link__menu_quick_links($variables) {

  $element = $variables['element'];
  $title = $element['#title'];
  $sub_menu = '';

  // global menu level variable.
  // within theme_menu_tree() at first level has always value = 1
  // but not here, that's why $depth will have to be defined separately
  global $level;
  $level = $element['#original_link']['depth'];

  // if you will need $depth, it must be defined separately
  // $depth = $element['#original_link']['depth'];

  // submenu
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  // link output
  $output = l($title, $element['#href'], $element['#localized_options']);

  // if link class is active, make li class as active too
  if(strpos($output,"active")>0){
    $element['#attributes']['class'][] = "active";
  }

  $attr = drupal_attributes($element['#attributes']);
  return  '<li' . $attr . '>' . $output .$sub_menu . "</li>\n";
}

function librarynih_facetapi_count($variables) {
  return '<span class="counter">(' . (int) $variables['count'] . ')</span>';
}
