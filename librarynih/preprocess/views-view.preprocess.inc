<?php

/**
 * Implements hook_preprocess_page().
 */
function librarynih_preprocess_views_view(&$vars) {
  $view = $vars['view'];
  if ($vars['name'] == 'list_features_home') {
    //dpm($vars);
    $vars['classes_array'][] = 'number-features-' . $view->total_rows;
  }

}
