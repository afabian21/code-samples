<div class="l-header-wrapper">
  <div class="l-page">
    <div class="l-header">
      <a  href="http://www.ors.od.nih.gov" title="Office of Research Services">
        <img src="/<?php print drupal_get_path('theme', 'librarynih') ?>/images/radialHilight2.jpg" alt="" />
      </a>
    </div>
    <div class="l-sub-header">
      <div class="l-ors_home">
         <?php print render($page['ors_home']); ?>
      </div>
      <div class="l-search">
         <?php print render($page['search']); ?>
      </div>
    </div>
  </div>
</div>
<div class="l-main-wrapper">
  <div<?php print $attributes; ?>>
    <div class="l-navigation">
       <?php print render($page['navigation']); ?>
    </div>
    <div class="l-main">
      <div class="l-content" role="main">
        <?php print render($page['highlighted']); ?>
        <?php print $breadcrumb; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div>

      <?php print render($page['sidebar_first']); ?>
      <?php print render($page['sidebar_second']); ?>
    </div>
    <p class="hours"><?php print "NIH Library in Building 10 Bethesda, MD 20892   301-496-1080   Today's Hours  7:45AM-8PM"?></p>
  </div>
</div>
<div class="l-footer-wrapper">
  <div class="l-page">
    <footer class="l-footer" role="contentinfo">
      <?php print render($page['footer']); ?>
    </footer>
  </div>
</div>
