name = Home for Library
description = Layout for the homepage of every library provided by NIH Library website.
preview = preview.png
template = homelibrary-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer
regions[ors_home]       = ORS Home
regions[search]         = Search


; Stylesheets
stylesheets[all][] = css/layouts/homelibrary/homelibrary.layout.css
