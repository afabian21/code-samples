name = 2 Columns Sidebar Content with Header
description = Two columns layout (Sidebar - Content) with header for the NIH Library site.
preview = preview.png
template = twocolumnhsc-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[header_content] = Header Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer
regions[ors_home]       = ORS Home
regions[search]         = Search
regions[banner]         = Banner

; Stylesheets
stylesheets[all][] = css/layouts/twocolumnhsc/twocolumnhsc.layout.css
