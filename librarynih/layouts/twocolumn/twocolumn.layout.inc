name = 2 Columns Sidebar Content
description = Two columns layout (Sidebar - Content) for the NIH Library site.
preview = preview.png
template = twocolumn-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer
regions[ors_home]       = ORS Home
regions[search]         = Search
regions[banner]         = Banner

; Stylesheets
stylesheets[all][] = css/layouts/twocolumn/twocolumn.layout.css
