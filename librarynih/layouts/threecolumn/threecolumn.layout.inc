name = 3 Columns
description = Three column layout for the NIH Library Website.
preview = preview.png
template = threecolumn-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer
regions[ors_home]       = ORS Home
regions[search]         = Search
regions[banner]         = Banner

; Stylesheets
stylesheets[all][] = css/layouts/threecolumn/threecolumn.layout.css
stylesheets[all][] = css/layouts/threecolumn/threecolumn.layout.no-query.css
