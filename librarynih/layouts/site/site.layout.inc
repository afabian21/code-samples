name = Site Template
description = Layout that contains the elements using in all the website (Header and footer).
preview = preview.png
template = site-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer
regions[ors_home]       = ORS Home
regions[search]         = Search


; Stylesheets
stylesheets[all][] = css/layouts/site/site.layout.css
stylesheets[all][] = css/layouts/site/site.layout.no-query.css
