name = 2 Columns Content Sidebar
description = Two columns layout (Content - Sidebar) for the NIH Library site.
preview = preview.png
template = twocolumncs-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer
regions[ors_home]       = ORS Home
regions[search]         = Search
regions[banner]         = Banner

; Stylesheets
stylesheets[all][] = css/layouts/twocolumncs/twocolumncs.layout.css
stylesheets[all][] = css/layouts/twocolumncs/twocolumncs.layout.no-query.css
