name = Home Site for NIH Library
description = Layout for the homepage of NIH Library website.
preview = preview.png
template = homesite-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer
regions[ors_home]       = ORS Home
regions[search]         = Search
regions[announcements]  = Announcements
regions[quick_blocks]   = Quick Blocks
regions[featured_ebooks]= Featured Ebooks

; Stylesheets
stylesheets[all][] = css/layouts/homesite/homesite.layout.css
stylesheets[all][] = css/layouts/homesite/homesite.layout.no-query.css
