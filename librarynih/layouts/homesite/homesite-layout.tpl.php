<div class="l-header-wrapper">
  <div class="l-page">
    <div class="l-header">
      <a  href="http://www.ors.od.nih.gov" title="Office of Research Services">
        <img src="/<?php print drupal_get_path('theme', 'librarynih') ?>/images/radialHilight2.jpg" alt="" />
      </a>
    </div>
    <div class="l-sub-header">
      <div class="l-ors_home">
         <?php print render($page['ors_home']); ?>
      </div>
      <div class="l-search">
         <?php print render($page['search']); ?>
      </div>
    </div>
  </div>
</div>
<div class="l-main-wrapper">
  <div<?php print $attributes; ?>>
    <div class="l-navigation">
       <?php print render($page['navigation']); ?>
    </div>
    <div class="l-main">
      <div class="l-content" role="main">
        <?php print render($page['highlighted']); ?>
        <?php print $breadcrumb; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div>

      <?php print render($page['sidebar_first']); ?>
      <?php print render($page['sidebar_second']); ?>
      <div class="announcements">
        <?php print render($page['announcements']); ?>
      </div>
      <div class="quick-blocks">
        <?php print render($page['quick_blocks']); ?>
      </div>
      <div class="featured-ebooks">
        <?php print render($page['featured_ebooks']); ?>
      </div>
    </div>

    <div class="social-link">
      <div class="column-fourth first-column">
        <a class="remote-access" href="http://ezproxy.nihlibrary.nih.gov/login">Remote Access</a>
      </div>
      <div class="column-fourth second-column">
        <a class="contact-us" href="mailto:nihlibrary@nih.gov">Contact Us</a>
      </div>
      <div class="column-fourth third-column">
        <a class="email-updates" href="http://nih.gov/email.htm">Email Updates</a>
      </div>
      <div class="column-fourth fourth-column">
        <a class="icon icon-fb" title="Facebook" href="https://www.facebook.com/nihlibrary">Facebook</a>
        <a class="icon icon-tw" title="Twitter" href="http://twitter.com/nihlib">Twitter</a>
        <a class="icon icon-yt" title="Youtube" href="http://twitter.com/nihlib">Youtube</a>
        <a class="icon icon-fc" title="Flickr" href="http://www.flickr.com/photos/nihlibrary/">Flickr</a>
      </div>
    </div>
    <p class="hours"><?php print "NIH Library in Building 10 Bethesda, MD 20892   301-496-1080   Today's Hours  7:45AM-8PM"?></p>
  </div>
</div>
<div class="l-footer-wrapper">
  <div class="l-page">
    <footer class="l-footer" role="contentinfo">
      <?php print render($page['footer']); ?>
    </footer>
  </div>
</div>
