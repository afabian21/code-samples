<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="articulo-resumen" data-equalizer>
<?php print $wrapper_prefix; ?>
	<h3 class="text-center">Temas de Inter&eacutes</h3>
   <?php print $list_type_prefix; ?>
<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes_array[$id]; ?>" data-equalizer-watch><?php print $row; ?></li>
    <?php endforeach; ?>
</ul>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
</div>