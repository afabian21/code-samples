<div class="text-center" data-equalizer>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
	<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3 text-center">
	    <?php foreach ($rows as $id => $row): ?>
	      <li class=" marca <?php print $classes_array[$id]; ?>" data-equalizer-watch><?php print $row; ?></li>
	    <?php endforeach; ?>
	</ul>
</div>