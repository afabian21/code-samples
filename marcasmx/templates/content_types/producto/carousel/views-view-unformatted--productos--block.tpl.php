<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="productos-carousel small-text-center">
<?php foreach ($rows as $id => $row): ?>
  <div class="<?php if ($classes_array[$id]) {print $classes_array;} ?> node-<?php print $view->result[$id]->nid; ?>">   
    <?php print $row; ?>
    </div>
<?php endforeach; ?>
</div>