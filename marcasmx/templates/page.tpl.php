<!--
 _____                         _ ______                      _        
|  __ \                       | |  ____|                    | |       
| |  | |_ __ _   _ _ __   __ _| | |__  __  ___ __   ___ _ __| |_ ___  
| |  | | '__| | | | '_ \ / _` | |  __| \ \/ / '_ \ / _ \ '__| __/ _ \ 
| |__| | |  | |_| | |_) | (_| | | |____ >  <| |_) |  __/ |  | || (_) |
|_____/|_|   \__,_| .__/ \__,_|_|______/_/\_\ .__/ \___|_|   \__\___/ 
                  | |                       | |                       
                  |_|                       |_|    
-->
<!--.page -->
<div role="document" class="page">

  <!--.l-header -->
  <header role="banner" class="l-header">
  	<div class="branding-wrapper visible-for-medium-up">
	  <div class="branding large-12 row">
	  	<div class="large-8 medium-6 columns small-only-text-center"><?php if ($linked_logo): print $linked_logo; endif; ?></div>
	  	<div class="large-4 medium-6 columns hide-for-small-only">
		  	<div class="social-links">
			  	<span><a class="twitter first" href="#">Twitter</a></span>
			  	<span><a class="facebook" href="#">Facebook</a></span>
			  	<span><a class="youtube" href="#">Youtube</a></span>
			  	<span><a class="blog last" href="#">Blog</a></span>
		  	</div>
	  	</div>
	  </div>
	  <?php if (!empty($page['header'])): ?>
      <!--.l-header-region -->
      <section class="l-header-region row hide-for-small-only">
        <div class="large-12 columns">
          <?php print render($page['header']); ?>
        </div>
      </section>
      <!--/.l-header-region -->
    <?php endif; ?>
  	</div>
    <?php if ($top_bar): ?>
      <!--.top-bar -->
      <?php if ($top_bar_classes): ?>
        <div class="<?php print $top_bar_classes; ?>">
      <?php endif; ?>
      <nav class="top-bar" data-topbar <?php print $top_bar_options; ?>>
        <ul class="title-area">
          <li class="logo-mobile visible-for-small-only"><?php if ($linked_logo): print $linked_logo; endif; ?></li>
          <li class="toggle-topbar menu-icon"><a href="#"><span><?php print $top_bar_menu_text; ?></span></a></li>
        </ul>
        <section class="top-bar-section">
          <?php if ($top_bar_main_menu) :?>
            <?php print $top_bar_main_menu; ?>
          <?php endif; ?>
          <?php if ($top_bar_secondary_menu) :?>
            <?php print $top_bar_secondary_menu; ?>
          <?php endif; ?>
        </section>
      </nav>
      <?php if ($top_bar_classes): ?>
        </div>
      <?php endif; ?>
      <!--/.top-bar -->
    <?php endif; ?>
  </header>
  <!--/.l-header -->
  
  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <!--.l-messages -->
    <section class="l-messages row">
      <div class="large-12 columns">
        <?php if ($messages): print $messages; endif; ?>
      </div>
    </section>
    <!--/.l-messages -->
  <?php endif; ?>

  <?php if (!empty($page['featured_1']) || !empty($page['featured_2']) || !empty($page['feature_3']) || !empty($page['feature_4'])): ?>
    <!--/.featured -->
    
    <section class="l-featured">
    <?php if (!empty($page['featured_1'])): ?>
    <div class="feature-1-wrapper">
    	<div class="">
	    	<div class="show-for-medium-up">
	        	<?php print render($page['featured_1']); ?>
	        </div>
	   </div>
    </div>
    <?php endif; ?>
    <div class="row">
	 	<div class="large-12 columns">
	      <div class="large-6 medium-6 columns">
	        <?php print render($page['featured_2']); ?>
	      </div>
	      <div class="large-6 medium-6 columns">
	        <?php print render($page['featured_3']); ?>
	      </div>
	 	</div>
    </div>
      <div class="row">
	      <div class="large-12 columns">
	        <?php print render($page['featured_4']); ?>
	      </div>
      </div>
    </section>
    
    <!--/.l-featured -->
  <?php endif; ?>

  <?php if (!empty($page['help'])): ?>
    <!--.l-help -->
    <section class="l-help row">
      <div class="large-12 columns">
        <?php print render($page['help']); ?>
      </div>
    </section>
    <!--/.l-help -->
  <?php endif; ?>
  
  <section class="l-before-content row">
      <div class="large-12 columns">
        <?php print render($page['before_content']); ?>
      </div>
   </section>

  <main role="main" class="row l-main">
    <div class="<?php print $main_grid; ?> main columns">
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlight panel callout">
          <?php print render($page['highlighted']); ?>
        </div>
      <?php endif; ?>

      <a id="main-content"></a>

      <?php if ($title && !$is_front): ?>
        <?php print render($title_prefix); ?>
        <h1 id="page-title" class="title"><?php print $title; ?></h1>
        <?php print render($title_suffix); ?>
      <?php endif; ?>

      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
        <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php if (!drupal_is_front_page()) { ?>
      	<?php print render($page['content']); ?>
      <?php } ?>
    </div>
    <!--/.l-main region -->

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside role="complementary" class="<?php print $sidebar_first_grid; ?> l-sidebar-first columns sidebar">
        <?php print render($page['sidebar_first']); ?>
      </aside>
    <?php endif; ?>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside role="complementary" class="<?php print $sidebar_sec_grid; ?> l-sidebar-second columns sidebar">
        <?php print render($page['sidebar_second']); ?>
      </aside>
    <?php endif; ?>
  </main>
  <!--/.l-main-->
  
  <section class="l-after-content row">
      <div class="large-12 columns">
        <?php print render($page['after_content']); ?>
      </div>
   </section>

  <!--.footer -->
<div role="footer" class="footer-wrapper last">

  <?php if (!empty($page['footer_firstcolumn']) || !empty($page['footer_secondcolumn']) || !empty($page['footer_thirdcolumn']) || !empty($page['footer_fourthcolumn'])): ?>
    <!--.footer-columns -->
    <section class="row l-footer-columns">
      <?php if (!empty($page['footer_firstcolumn'])): ?>
        <div class="footer-first large-12 columns">
          <?php print render($page['footer_firstcolumn']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_secondcolumn'])): ?>
        <div class="footer-second large-4 medium-4 columns text-center">
          <?php print render($page['footer_secondcolumn']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_thirdcolumn'])): ?>
        <div class="footer-third large-8 medium-8 columns">
          <?php print render($page['footer_thirdcolumn']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_fourthcolumn'])): ?>
        <div class="footer-fourth large-12 columns">
          <?php print render($page['footer_fourthcolumn']); ?>
        </div>
      <?php endif; ?>
    </section>
    <!--/.footer-columns-->
  <?php endif; ?>

  <!--.l-footer-->
  <footer class="l-footer row" role="contentinfo">
    <?php if (!empty($page['footer'])): ?>
      <div class="footer large-12 columns">
        <?php print render($page['footer']); ?>
      </div>
    <?php endif; ?>
  </footer>
  <!--/.footer-->
  
  <?php if (!empty($page['slidebox'])): ?>
        <div id='slidebox' class="slidebox callout">
	        <a class="close"></a>
          <?php print render($page['slidebox']); ?>
        </div>
 <?php endif; ?>
 
  <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->