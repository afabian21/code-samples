<?php
function ds_one_col_header_footer() {
  return array(
    'label' => t('One Col Header and Footer'),
    'regions' => array(
      'header' => t('Header'),
      'main' => t('Main'),
      'footer' => t('Footer'),
    ),
    // Add this line if there is a default css file.
    'css' => FALSE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}