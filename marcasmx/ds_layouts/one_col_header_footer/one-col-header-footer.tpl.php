<?php

/**
 * @file
 * Display Suite 1 column template with layout wrapper.
 */
?>
<div class="<?php print $classes;?> clearfix">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>
  <?php if ($header): ?>
    <div class="ds-header row<?php print $header_classes; ?>">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
  <?php if ($main): ?>
    <div class="ds-main row social-links<?php print $main_classes; ?>">
      <?php print $main; ?>
    </div>
  <?php endif; ?>
  <?php if ($footer): ?>
    <div class="ds-footer row<?php print $footer_classes; ?>">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>
</div>
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>