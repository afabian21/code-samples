(function ($, Drupal) {

  Drupal.behaviors.marcasmx = {
    attach: function(context, settings) {
      // Get your Yeti started.
      
       //Slidebox
      
      jQuery.fn.slidebox = function() {
    var slidebox = this;
    var originalPosition = slidebox.css('right');
    var open = false;

    /* GA tracking */
    var track = function(label) {
      return _gaq.push(['_trackEvent', 'Slidebox', label]);
    }
    var boxAnimations;

    if (Modernizr.cssanimations) {
      boxAnimations = {
        open:  function() { slidebox.addClass('open'); },
        close: function() { slidebox.removeClass('open'); },
      }
    } else {
      boxAnimations = {
        open: function() {
          slidebox.animate({
            'right': '0px'
          }, 300);
        },
        close: function() {
          slidebox.stop(true).animate({
            'right': originalPosition
          }, 100);
        }
      }
    }

    $(window).scroll(function() {
      var distanceTop = $('.l-after-content').offset().top - $(window).height();
      var distanceClose = $('.footer-wrapper').offset().top - $(window).height();
      if ($(window).scrollTop() > distanceTop && $(window).scrollTop() < distanceClose ) {
        if (!open) {
          open = true;
          boxAnimations.open();
          track("Open");
        }
      } else {
        open = false;
        boxAnimations.close();
      }
    });

    slidebox.find('.close').click(function() {
      $(this).parent().remove();
      track("Close");
    });
    slidebox.find('.related a').click(function() {
      track("Read More");
    });
  }

  function showPromoScreen($widthScreen) { 
	  
	 
	  
	  if ($widthScreen >= "420") {
		  
	  }
	  
	  
	  
  }
  
  
   var widthScreen = $(window).width();
           $(window).ready(showPromoScreen(widthScreen)).resize(
               function () {
                   var widthScreen = $(window).width();
                   showPromoScreen(widthScreen);
               }
           );
           
$(function(){
		  	$('#slidebox').slidebox();
		  });
  //end

      
      
      //Agrega clase first y last a .social-links
      $.each($('.social-links'), function() {
      	$('span:first-of-type').addClass('first');
      	$('span:last-of-type').addClass('last');
       });
      }
      
      
      
      
  };

})(jQuery, Drupal);
