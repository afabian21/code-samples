(function ($) {
  // Original JavaScript code.
  $(document).ready(function() {
  
  
    $('.slideshow-top').slick({
    	arrows: false,
        dots: true,
        infinite: true,
        speed: 400,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2750,
        fade: true,
    });
    
    
    var $carousel = $('.productos-carousel');
    
    function showSliderScreen($widthScreen) {
	    
	    //console.log($widthScreen);
	    
	    if ($widthScreen >= "420") {
		    
		    if (!$carousel.hasClass('slick-initialized')) {
		    
			    
				$('.productos-carousel').slick({
			      dots: true,
				  infinite: false,
				  speed: 300,
				  slidesToShow: 4,
				  slidesToScroll: 4,
				  autoplay: true,
				  autoplaySpeed: 3500,
				  responsive: [
				  	{
					  breakpoint: 1024,
					  settings: {
						  	slidesToShow: 3,
						  	slidesToScroll: 3,
						  	infinite: true,
						  	dots: true
						  	}
						  },
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
						}
						]
		    });
		    
		    	 //Cuenta elementos 
		    	 //$elementos = $('.productos-carousel .slick-track .Array').length;
		    	 //$bordes = $elementos - 1;
		    	//console.log($bordes);
		    
		    }//End If
		    
	    } else {
                   if ($carousel.hasClass('slick-initialized')) {
                       $carousel.unslick();
                   }
                }   

    }
    
    var widthScreen = $(window).width();
           $(window).ready(showSliderScreen(widthScreen)).resize(
               function () {
                   var widthScreen = $(window).width();
                   showSliderScreen(widthScreen);
               }
           );

   
});//termina ready

})(jQuery);