<?php
/**
 * @file
 * Provides the charts definitions.
 */

function graficas_tables() {
  $period = _obtener_fecha_de_url();
  $marca = _obtener_marca_de_url();
  $from = isset($period[0]) ? $period[0] : strtotime('-1 month');
  $to   = isset($period[1]) ? $period[1] : time();
  $header = $nodes = $comments = $votes = $users = array();
  
  //print_r("tables");
  /*preguntas no respondidas por marca*/
  if (module_exists('node')) {
    $j = 0;
    // Building the rows, array of the data point arrays. 
    $rows1 = array();
    $rows = array(); //se tiene que volver a setear $rows para utilizarlo limpio
    $ret = '<p><table class="views-table sticky-enabled cols-2 tableheader-processed sticky-table">';
    $ret .= '<thead><tr><th>Marca</th><th>Preguntas sin responder</th><tr></thead>';
    $ret .= '<tbody>';
    $query_marcas = db_query("SELECT tid, name FROM {taxonomy_term_data} WHERE vid = 4");
    while ($nombre_marcas = $query_marcas->fetchAssoc()){
      $total_questions = 0;
      $total_answers = 0;
      $sin_respuesta = 0;
      $i = 0;
      $f = $from;
      
      //while ($f <= $to) {
        // Building the header - list of date from today backward.
        $fecha[$i] = date('d.m.y', $f);
        // The number of nodes created each day.
        $nodes_question[$i] = db_query("SELECT COUNT(*) FROM {node} n
          JOIN {field_data_field_marca_taxonomia} m
          WHERE ( n.type = 'question'
            AND n.nid = m.entity_id
            AND m.field_marca_taxonomia_tid = :marca)",
            array(':marca' => $nombre_marcas['tid']))->fetchField();
  
        $nodes_answer[$i] = db_query("SELECT COUNT(*) FROM {field_data_field_answer_question} q
          JOIN {field_data_field_marca_taxonomia} m
          JOIN {node} n
          WHERE ( q.field_answer_question_nid = m.entity_id
          AND q.field_answer_question_nid = n.nid
          AND m.field_marca_taxonomia_tid = :marca)", 
          array(':marca' => $nombre_marcas['tid']))->fetchField();

        $total_questions = $total_questions + $nodes_question[$i];
        $total_answers = $total_answers + $nodes_answer[$i];

        $i ++;
        $f = strtotime("+1 day", $f);   
      //}
      $sin_respuesta = $total_questions - $total_answers;
      $header[$j] = $nombre_marcas['name'];
      $rows1[] = $sin_respuesta; 
      $j ++;
      if($j % 2) {
        $ret .= '<tr class="odd">';
      } else {
        $ret .= '<tr class="even">';
      }
      $ret .= '<td>'. $nombre_marcas['name'] .'</td>';
      $ret .= '<td>'. $sin_respuesta .'</td>';
      $ret .= '</tr></p>';
    };

    
    $ret .= '<tbody>';
    $ret .= "</table>";
  }
  
  /*preguntas no respondidas por marca*/
  return $ret;
}