<?php
/**
 * @file
 * Provides the charts definitions.
 */

function graficas_charts() {
  $period = _obtener_fecha_de_url();
  $marca = _obtener_marca_de_url();
  $query_nombre_marca = db_query("SELECT name FROM {taxonomy_term_data} WHERE tid = :marca", 
  array(':marca' => $marca));
  $nombre_marca = $query_nombre_marca->fetchCol();
  if($marca != "" || $marca != 0) {
    $query_string = "AND m.field_marca_taxonomia_tid = ".$marca;
    $brand_text_string = "de la marca: ".$nombre_marca[0];
  }
  else {
    $query_string = "";
    $brand_text_string = "de todas las marcas";
  }

  $from = isset($period[0]) ? $period[0] : strtotime('-1 month');
  $to   = isset($period[1]) ? $period[1] : time();
  $header = $nodes = $comments = $votes = $users = array();
  //==================================
  // Chart for Nodes created per day.
  //==================================
  if (module_exists('node')) {
    $i = 0;
    $f = $from;
    $counter = 0;
    while ($f <= $to) {
      // Building the header - list of date from today backward.
      $header[$i] = date('d.m.y', $f);
      // The number of nodes created each day.
      $nodes[$i] = db_query("SELECT COUNT(*) FROM {node} n
        JOIN {field_data_field_marca_taxonomia} m
        WHERE ( n.type = 'question'
          AND n.nid = m.entity_id
          ".$query_string."
          AND FROM_UNIXTIME(n.created,'%d.%m.%y') = :dete )",
      array(':dete' => $header[$i]))->fetchField();
      $counter = $counter + $nodes[$i];
      $i ++;      
      $f = strtotime("+1 day", $f);
    }
    // Building the rows, array of the data point arrays.
    $rows = array($nodes);
    // The labels for the rows.
    $columns = array($counter.' Preguntas');

    // Put all the data into the settings array, 
    // which will be send to draw.

    // Must empty the array first.
    $settings = array();
    $settings['chart']['chartNodes'] = array(  
      'header' => $header,
      'rows' => $rows,
      'columns' => $columns,
      'chartType' => 'LineChart',// LineChart, PieChart, ,ColumnChart, ,AreaChart, Gauge, BarChart, etc....
      'options' => array( // Optionals.
        'curveType' => "function",
        'forceIFrame' => FALSE,  
        'title' => 'Preguntas enviadas por dia '.$brand_text_string,
        'width' => 900,
        'height' => 580  
      )   
    );

    //Draw it.
    $ret[] = draw_chart($settings);
  }
/**/
/*respuestas por día*/
/**/
if (module_exists('node')) {
    $i = 0;
    $f = $from;
    $counter = 0;
    while ($f <= $to) {
      // Building the header - list of date from today backward.
      $header[$i] = date('d.m.y', $f);
      // The number of nodes created each day.
      $nodes[$i] = db_query("SELECT COUNT(*) FROM {field_data_field_answer_question} q
        JOIN {field_data_field_marca_taxonomia} m
        JOIN {node} n
        WHERE ( q.field_answer_question_nid = m.entity_id
        AND q.field_answer_question_nid = n.nid
        ".$query_string."
        AND FROM_UNIXTIME(n.created,'%d.%m.%y') = :dete )", 
        array(':dete' => $header[$i]))->fetchField();
      $counter = $counter + $nodes[$i];
      $i ++;
      $f = strtotime("+1 day", $f);
    }
    //print_r($rows);
    // Building the rows, array of the data point arrays.
    $rows = array($nodes);
    // The labels for the rows.
    $columns = array($counter.' Respuestas');

    // Put all the data into the settings array, 
    // which will be send to draw.

    // Must empty the array first.
    $settings = array();
    $settings['chart']['chartNodes'] = array(  
      'header' => $header,
      'rows' => $rows,
      'columns' => $columns,
      'chartType' => 'LineChart',// LineChart, PieChart, ,ColumnChart, ,AreaChart, Gauge, BarChart, etc....
      'options' => array( // Optionals.
        'curveType' => "function",
        'forceIFrame' => FALSE,  
        'title' => 'Respuestas enviadas por dia '.$brand_text_string,
        'width' => 900,
        'height' => 580  
      )   
    );

    //Draw it.
    $ret[] = draw_chart($settings);
  }

/*preguntas vs respuestas*/

if (module_exists('node')) {
    $total = 0;
    $i = 0;
    $f = $from;
    while ($f <= $to) {
      // Building the header - list of date from today backward.
      $fecha[$i] = date('d.m.y', $f);
      // The number of nodes created each day.
      $nodes_question[$i] = db_query("SELECT COUNT(*) FROM {node} n
        JOIN {field_data_field_marca_taxonomia} m
        WHERE ( n.type = 'question'
          AND n.nid = m.entity_id
          ".$query_string."
          AND FROM_UNIXTIME(n.created,'%d.%m.%y') = :dete )",
          array(':dete' => $fecha[$i]))->fetchField();
        
      $nodes_answer[$i] = db_query("SELECT COUNT(*) FROM {field_data_field_answer_question} q
        JOIN {field_data_field_marca_taxonomia} m
        JOIN {node} n
        WHERE ( q.field_answer_question_nid = m.entity_id
        AND q.field_answer_question_nid = n.nid
        ".$query_string."
        AND FROM_UNIXTIME(n.created,'%d.%m.%y') = :dete )", 
        array(':dete' => $fecha[$i]))->fetchField();

      $total_questions = $total_questions + $nodes_question[$i];
      $total_answers = $total_answers + $nodes_answer[$i];

      $i ++;
      $f = strtotime("+1 day", $f);   
    }

    // Building the rows, array of the data point arrays.
    $sin_respuesta = $total_questions - $total_answers;  
    $rows = array(); //se tiene que volver a setear $rows para utilizarlo limpio
    $rows[] = array($total_answers, $sin_respuesta);
    $header = array('Respondidas '.$total_answers.'','Sin Responder '.$sin_respuesta.'');
    // The labels for the rows.
    $columns = array('No. Of Contents');
    $settings = array();
    $settings['chart']['chartQuestions'] = array(  
      'header' => $header,
      'rows' => $rows,
      'columns' => $columns,
      'chartType' => 'PieChart',//LineChart,PieChart,ColumnChart,AreaChart,Gauge,BarChart,etc....
      'options' => array( // Optionals.
        'curveType' => "function",
        'forceIFrame' => FALSE,  
        'title' => 'Preguntas '.$brand_text_string.' '.$total_questions,
        'width' => 500,
        'height' => 300  
      )   
    );
    //Draw it.
    $ret[] = draw_chart($settings);
  }
  
  //=====================================
  // Chart for user joind per day.
  //=====================================
  if (module_exists('user')) {
    $i = 0;
    $f = $from;
    while ($f <= $to) {
      // Building the header - list of date from today backward.
      $header[$i] = date('d.m.y', $f);
      // The number of users join each day.
      
      $users[$i] = db_query("SELECT COUNT(*) FROM {users} u 
        JOIN {domain_editor} d 
        JOIN {node} n 
        JOIN {field_data_field_marca_taxonomia} m 
        JOIN {node_type} t 
                              WHERE ( d.domain_id = 5 
                                AND u.uid = d.uid 
                                AND u.uid = n.uid 
                                AND n.nid = m.entity_id 
                                AND t.type = 'question' 
                                AND n.type = t.type 
                                ".$query_string."
                                AND FROM_UNIXTIME(u.created,'%d.%m.%y') = :dete )", 
                                array(':dete' => $header[$i]))->fetchField();
      $i ++;
      $f = strtotime("+1 day", $f);
    }
    // Building the rows, array of the data point arrays.
    $rows = array($users);
    // The labels for the rows.
    $columns = array('No. de Usuarios');

    // Put all the data into the settings array, 
    // which will be send to draw.

    // Must empty the array first.
    $settings = array();
    $settings['chart']['chartUsers'] = array(  
      'header' => $header,
      'rows' => $rows,
      'columns' => $columns,
      'chartType' => 'LineChart',// LineChart, PieChart, ,ColumnChart, ,AreaChart, Gauge, BarChart, etc....
      'options' => array( // Optionals.
        'curveType' => "function",
        'colors' => array('orange'),   
        'forceIFrame' => FALSE,  
        'title' => 'Usuarios registrados por dia '.$brand_text_string,
        'width' => 900,
        'height' => 580  
      )   
    );

    //Draw it.
    $ret[] = draw_chart($settings); 
  }  
  return $ret;
}