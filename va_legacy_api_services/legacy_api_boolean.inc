<?php

function legacy_api_boolean_services_resource() {
	$api = array(
		'legacy_member_boolean' => array(
		  	'operations' => array(
		    	'retrieve' => array(
			      	'help' => 'Retrieves users membership info.',
			      	'file' => array(
			        	'type' => 'inc',
			        	'module' => 'legacy_api_boolean',
			        	'name' => 'legacy_api_boolean',
			      	),
			      	'callback' => 'legacy_api_boolean_services_resource_retrieve',
			      	'access callback' => 'user_access',
			      	'access arguments' => array('access content'),
			      	'access arguments append' => false,
			      	'args' => array(
			        	array(
				          	'name' => 'none',
				          	'type' => 'string',
				          	'description' => 'Function to perform',
				          	'source' => array(
				            	'path' => '0'
				          	),
				          	'optional' => TRUE,
				          	'default' => '0',
			        	),
			        	array(
				          	'name' => 'version',
				          	'type' => 'int',
				          	'description' => 'Function to perform',
				          	'source' => array(
				            	'param' => 'version'
				          	),
				          	'optional' => TRUE,
				          	'default' => '0',
			        	),
			        	array(
				          	'name' => 'username',
				          	'type' => 'string',
				          	'description' => 'Function to perform',
				          	'source' => array(
				            	'param' => 'username'
				          	),
				          	'optional' => FALSE,
				          	'default' => '0',
			        	),
			        	array(
				          	'name' => 'password',
				          	'type' => 'string',
				          	'description' => 'Function to perform',
				          	'source' => array(
				            	'param' => 'password'
				          	),
				          	'optional' => FALSE,
				          	'default' => '0',
			        	),
			        	array(
				          	'name' => 'id',
				          	'type' => 'string',
				          	'description' => 'Function to perform',
				          	'source' => array(
				            	'param' => 'id'
				          	),
				          	'optional' => TRUE,
				          	'default' => '0',
			        	),
			      	),
			  	),
			),
		),
	);
	return $api;
}

/**
 * [legacy_api_boolean_services_resource_retrieve] definition.
 * Returns the information about a user account.
 * @param $id
 *   The id of the user.
 * @return boolean value 0,1
 *	The true false value of a members status.
 */
function legacy_api_boolean_services_resource_retrieve ($none, $version, $username, $password, $id) {
	$testhash = sha1($username.'+salt');

	if ($password == $testhash) {

		if (is_numeric($id)) {
			//Query for travel codes
			$query = "SELECT entity_id FROM {field_data_field_user_travel_code} WHERE field_user_travel_code_value = '".$id."' ORDER BY entity_id DESC LIMIT 1";
	//		error_log($query);
			//Query result
			$result = db_query($query);
	//		error_log(print_r($result));
			//Set a default uid in case of failure
			$uid = 0;

			if($result) {
				while($row = $result->fetchAssoc()) {
					//Replace uid with the entity id resulting from the query
					$uid = $row['entity_id'];
				}
			} else {
				//Error handling
				//Not sure what you want in here
			}

			//The query did not come back with a user profile
			if($uid == 0) {
				return 0;
			}

			//Load the user based on the entity id received by the query
			$user = user_load($uid);
		} else {
			$user = user_load_by_name($id);
		}

		$role = user_role_load_by_name('administrator');//"VetRewards Members");

		$rid = $role->rid;

		if(user_has_role($rid, $user)) {
			return true;
		} else {
			return 0;
		}
	} else {
		return "Invalid Credentials";
	}
}
/*
function legacy_api_boolean_services_resource_retrieve_travel($id) {
	//Query for travel codes
	$query = "SELECT entity_id FROM {field_data_field_user_travel_id_data} WHERE field_user_travel_code_value = '".$id."' ORDER BY entity_id DESC LIMIT 1";

	//Query result
	$result = db_query($query);

	//Set a default uid in case of failure
	$uid = 0;

	if($result) {
		while($row = $result->fetchAssoc()) {
			//Replace uid with the entity id resulting from the query
			$uid = $result['entity_id'];
		}
	} else {
		//Error handling
		//Not sure what you want in here
	}

	//The query did not come back with a user profile
	if($uid == 0) {
		return 0;
	}

	//Load the user based on the entity id received by the query
	$user = user_load($uid);

	//Load the role name
	$role = user_role_load_by_name("administrator");
	$rid = $role->rid; //Get the role ID

	if(user_has_role($rid, $user)) { //Check to see if member is has a VetRewards membership
		return true;
	} else {
		return 0;
	}
}
/*
//Assumes you have the UID
$user = user_load($uid);

//OR you can do it by name
$user = user_load_by_name($name);

$user_wrapper = entity_metadata_wrapper('user', $user);
$travel_code = $user_wrapper->field_user_amtrak_id->value();
*/

?>